#include "task.h"

Task::Task()
{

}

Task::Task(QString _type, QString _name, QString _comment, QDateTime _startDate, QDateTime _endDate) :
    type(_type), name(_name), comment(_comment), startDate(_startDate), endDate(_endDate)
{

}

Task::~Task()
{

}

void        Task::setType(QString _type)
{
    type = _type;
}

void        Task::setName(QString _name)
{
    name = _name;
}

void        Task::setComment(QString _comment)
{
    comment = _comment;
}

void        Task::setStartDate(QDateTime _startDate)
{
    startDate = _startDate;
}

void        Task::setEndDate(QDateTime _endDate)
{
    endDate = _endDate;
}


QString     Task::getType()
{
    return (type);
}

QString     Task::getName()
{
    return (name);
}

QString     Task::getComment()
{
    return (comment);
}

QDateTime   Task::getStartDate()
{
    return (startDate);
}

QDateTime   Task::getEndDate()
{
    return (endDate);
}
