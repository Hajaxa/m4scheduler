#include "log.h"
#include "ui_log.h"

Log::Log(QVector<QPair<QString,QString>> _credentials, QWidget *parent) :
    credentials(_credentials),
    QDialog(parent),
    ui(new Ui::Log)
{
    ui->setupUi(this);

    this->timer = new QTimer(this);
    connect(this->timer, SIGNAL(timeout()), this, SLOT(displayError()));
}

Log::~Log()
{
    delete ui;
}

void    Log::checkCredential(QString username, QString password)
{
    if (password.isEmpty() || username.isEmpty())
        this->timer->start(60);

    for (int i = 0; i < this->credentials.size(); ++i)
    {
        if (username == this->credentials[i].first && password == this->credentials[i].second)
        {
            this->ui->label_2->setText("Connection successful");
            this->ui->label_2->setStyleSheet("QLabel { color: rgb(30, 175, 30, 255); }");
            this->user = username;
            QDialog::accept();
        }
    }
    this->ui->passwordLineEdit->clear();
    this->timer->start(60);
}

QString Log::getUser()
{
    return (this->user);
}

void    Log::on_buttonBox_accepted()
{
    this->checkCredential(this->ui->usernameLineEdit->text(), this->ui->passwordLineEdit->text());
}

void    Log::displayError()
{
    QLineEdit   *line;
    static int  i = 0;

    this->ui->label_2->setStyleSheet("QLabel { color: rgb(255, 30, 30, 200); }");

    if (this->ui->usernameLineEdit->text().isEmpty())
    {
        line = this->ui->usernameLineEdit;
        this->ui->label_2->setText("Incorrect username");
    }
    else
    {
        line = this->ui->passwordLineEdit;
        this->ui->label_2->setText("Incorrect username or password");
    }

    if (i >= 6)
    {
        i = 0;
        this->timer->stop();
        return ;
    }
    if (i % 2 == 0)
        line->setStyleSheet("QLineEdit { background: rgb(255, 30, 30, 150); }");
    else if (i % 2 == 1)
        line->setStyleSheet("QLineEdit { background: rgb(255, 255, 255); }");
    ++i;
}
