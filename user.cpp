#include "user.h"

User::User()
{

}

User::~User()
{

}

void                User::setName(QString _name)
{
    name = _name;
}

void                User::setGroupNames(QVector<QString> _groupNames)
{
    groupNames = _groupNames;
}

void                User::setPower(int _power)
{
    if (_power > power)
        power = _power;
}

void                User::setTasks(QVector<Task*> _tasks)
{
    tasks = _tasks;
}

void                User::setCity(QString _city)
{
    city = _city;
}

void                User::addGroupName(QString _groupName)
{
    groupNames.push_back(_groupName);
}

void                User::addTask(Task *_task)
{
    tasks.push_back(_task);
}


QString             User::getName()
{
    return (name);
}

QVector<QString>    User::getGroupNames()
{
    return (groupNames);
}

int                 User::getPower()
{
    return (power);
}

QVector<Task*>      User::getTasks()
{
    return (tasks);
}

QString             User::getCity()
{
    return (city);
}
