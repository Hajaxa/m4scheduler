#ifndef LOG_H
#define LOG_H

#include <QDialog>
#include <QVector>
#include <QPair>
#include <unistd.h>
#include <QTimer>

namespace Ui {
class Log;
}

class Log : public QDialog
{
    Q_OBJECT

public:
    explicit Log(QVector<QPair<QString,QString>> _credentials, QWidget *parent = 0);
    ~Log();

    void            checkCredential(QString username, QString password);
    QString         getUser();

private slots:
    void            on_buttonBox_accepted();
    void            displayError();

private:
    Ui::Log         *ui;
    QTimer          *timer;
    QVector<QPair<QString, QString>>    credentials;
    QString         user;
};

#endif // LOG_H
