#ifndef TASK_H
#define TASK_H

#include <QString>
#include <QDateTime>

class Task
{
public:
    Task();
    Task(QString _type, QString _name, QString _comment, QDateTime _startDate, QDateTime _endDate);
    ~Task();

    void        setType(QString _type);
    void        setName(QString _name);
    void        setComment(QString _comment);
    void        setStartDate(QDateTime _startDate);
    void        setEndDate(QDateTime _endDate);

    QString     getType();
    QString     getName();
    QString     getComment();
    QDateTime   getStartDate();
    QDateTime   getEndDate();

private:
    QString     type;
    QString     name;
    QString     comment;
    QDateTime   startDate;
    QDateTime   endDate;
};

#endif // TASK_H
