#ifndef GROUP_H
#define GROUP_H

#include "user.h"

class Group
{
public:
    Group();
    Group(QString _name, int _power);
    ~Group();

    void            setName(QString _name);
    void            setUsers(QVector<User*> _users);
    void            setPower(int _power);

    void            addUser(User *_user);

    QString         getName();
    QVector<User*>  getUsers();
    int             getPower();

private:
    QString         name;
    QVector<User*>  users;
    int             power;
};

#endif // GROUP_H
