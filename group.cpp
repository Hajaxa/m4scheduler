#include "group.h"

Group::Group()
{
}

Group::Group(QString _name, int _power)
    : name(_name), power(_power)
{
}

Group::~Group()
{
}

void            Group::setName(QString _name)
{
    name = _name;
}

void            Group::setUsers(QVector<User*> _users)
{
    users = _users;
}

void            Group::setPower(int _power)
{
        power = _power;
}

void            Group::addUser(User *_user)
{
    users.push_back(_user);
}

QString         Group::getName()
{
    return (name);
}

QVector<User*>  Group::getUsers()
{
    return (users);
}

int             Group::getPower()
{
    return (power);
}
