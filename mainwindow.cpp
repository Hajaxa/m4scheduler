#include "mainwindow.h"
#include "ui_mainwindow.h"

#define CIT "CITY"
#define GRP "GROUP"
#define TYP "TYPE"

MainWindow::MainWindow(QString username, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->generateTestData();
    this->start(username);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void                MainWindow::generateTestData()
{
    QVector<QString>        vecGroups   = {"Admin", "Manager", "Pleb"};
    QVector<int>            vecPower    = {100, 50, 1};
    QVector<User*>          vecUsers;

    QVector<QString>        vecNames    = {"M4ADM", "M1101", "Paco", "Kami", "Employee 1"};
    QVector<QString>        vecCity     = {"Paris", "Carvin", "San Francisco", "Sora", "Paris"};
    QVector<QVector<Task*>> vecTasks    = {{
                                               new Task("Administration",   "Create jobs",      "Create jobs for newcomers",
                                                        QDateTime(QDate(2017, 11, 1), QTime(9, 0, 0)),
                                                        QDateTime(QDate(2017, 11, 4), QTime(18, 0, 0))),
                                               new Task("Intervention",     "Check everything", "Don't miss anyone",
                                                        QDateTime(QDate(2017, 11, 2), QTime(10, 0, 0)),
                                                        QDateTime(QDate(2017, 11, 3), QTime(12, 0, 0))),
                                               new Task("Break",            "Coffee time",      "Drink with your co-workers",
                                                        QDateTime(QDate(2017, 11, 1), QTime(14, 30, 0)),
                                                        QDateTime(QDate(2017, 11, 1), QTime(14, 40, 0)))
                                           },
                                           {
                                               new Task("Break",            "Coffee ?",         "Give me moar sugar pls",
                                                        QDateTime(QDate(2017, 11, 1), QTime(10, 00, 0)),
                                                        QDateTime(QDate(2017, 11, 7), QTime(16, 00, 0)))
                                           },
                                           {
                                               new Task("Hard work",        "Pay Process",      "1st pay process of September",
                                                        QDateTime(QDate(2017, 11, 1), QTime(8, 55, 0)),
                                                        QDateTime(QDate(2017, 11, 9), QTime(23, 55, 0))),
                                               new Task("Hard work",        "GTA",              "GTA",
                                                        QDateTime(QDate(2017, 11, 10), QTime(15, 55, 0)),
                                                        QDateTime(QDate(2017, 11, 14), QTime(16, 55, 0))),
                                               new Task("Meeting",          "Meet Kami",        "Meet the almighty.",
                                                        QDateTime(QDate(2017, 11, 15), QTime(6, 0, 0)),
                                                        QDateTime(QDate(2017, 11, 15), QTime(18, 30, 0)))
                                           },
                                           {
                                               new Task("RH",               "Judge",            "Just. Judge. People.",
                                                        QDateTime(QDate(2017, 11, 1), QTime(0, 0, 0)),
                                                        QDateTime(QDate(2017, 11, 30), QTime(23, 59, 0))),
                                               new Task("Meeting",          "Invite Paco",      "Meeting with the pleb",
                                                        QDateTime(QDate(2017, 11, 15), QTime(6, 0, 0)),
                                                        QDateTime(QDate(2017, 11, 15), QTime(18, 55, 0)))

                                           },
                                           {
                                               new Task("Hard work",        "TMA",              "TMA",
                                                        QDateTime(QDate(2017, 11, 15), QTime(9, 30, 0)),
                                                        QDateTime(QDate(2017, 11, 25), QTime(17, 30, 0))),
                                               new Task("Intervention",     "At Babilou's",     "Demonstration of calendar",
                                                        QDateTime(QDate(2017, 11, 17), QTime(10, 0, 0)),
                                                        QDateTime(QDate(2017, 11, 17), QTime(15, 0, 0))),
                                               new Task("Break",            "It's time.",       "Time to drink your soul.",
                                                        QDateTime(QDate(2017, 11, 1), QTime(14, 30, 0)),
                                                        QDateTime(QDate(2017, 11, 1), QTime(14, 40, 0)))
                                           }
                                          };

    for (int i = 0; i < vecGroups.size(); ++i)
        this->groups.push_back(new Group(vecGroups[i], vecPower[i]));

    for (int i = 0; i < vecNames.size(); ++i)
    {
        User            *user = new User();
        user->setName(vecNames[i]);
        user->setTasks(vecTasks[i]);
        user->setCity(vecCity[i]);

        if (vecNames[i] == "M4ADM" || vecNames[i] == "Kami")
        {
            user->addGroupName(groups[0]->getName());
            user->setPower(groups[0]->getPower());
        }
        else if (vecNames[i] == "M1101")
        {
            user->addGroupName(groups[1]->getName());
            user->setPower(groups[1]->getPower());
        }
        else
        {
            user->addGroupName(groups[2]->getName());
            user->setPower(groups[2]->getPower());
        }
        vecUsers.push_back(user);
    }

    for (int i = 0; i < vecUsers.size(); ++i)
    {
        for (int l = 0; l < groups.size(); ++l)
        {
            for (int n = 0; n < vecUsers[i]->getGroupNames().size(); ++n)
            {
                if (vecUsers[i]->getGroupNames()[n] == groups[l]->getName())
                {
                    groups[l]->addUser(vecUsers[i]);
                    break;
                }
            }
        }
    }
    vecGroups.insert(0, "Myself");
    outputData();
}

void                MainWindow::outputData()
{
    QString         output;
    QTextStream     out(&output);

    for (int i = 0; i < this->groups.size(); ++i)
    {
        Group       *group = this->groups[i];

        out << group->getName() << ":" << '\n';
        out << "\tPower = " << QString::number(group->getPower()) << "\n";
        out << "\tUsers: \n";
        for (int n = 0; n < group->getUsers().size(); ++n)
        {
            User    *user = group->getUsers()[n];

            out << "\t\tName = "    << user->getName()  << "\n";
            out << "\t\tPower = "   << user->getPower() << "\n";
            out << "\t\tCity = "    << user->getCity()  << "\n";
            out << "\t\tGroups = ";
            for (int l = 0; l < user->getGroupNames().size(); ++l)
            {
                out << user->getGroupNames()[l];
                if (l + 1 < user->getGroupNames().size())
                    out << ", ";
                else
                    out << "\n";
            }
            out << "\t\tTasks: \n\t\t{\n";
            for (int l = 0; l < user->getTasks().size(); ++l)
            {
                Task    *task = user->getTasks()[l];

                out << "\t\t\tType = "          << task->getType()                  << "\n";
                out << "\t\t\tName = "          << task->getName()                  << "\n";
                out << "\t\t\tComment = "       << task->getComment()               << "\n";
                out << "\t\t\tStart date = "    << task->getStartDate().toString()  << "\n";
                out << "\t\t\tEnd date = "      << task->getEndDate().toString()    << "\n\t\t}\n";
            }
            out << "\n";
        }
        out << "\n";
    }
    std::cout << output.toStdString();
}

// RESUME HERE

void                MainWindow::start(QString username)
{
    User            *user = this->findUser(username);

    this->currentUser = user;
    this->loadWeakerUsers(user);

    filteredUsers = loadedUsers;

    this->fillUI();
}

User                *MainWindow::findUser(QString username)
{
    for (int i = 0; i < groups.size(); ++i)
    {
        for (int n = 0; n < groups[i]->getUsers().size(); ++n)
        {
            if (groups[i]->getUsers()[n]->getName() == username)
                return (groups[i]->getUsers()[n]);
        }
    }
    return (NULL);
}

void                MainWindow::loadWeakerUsers(User *user)
{
    for (int i = 0; i < groups.size(); ++i)
    {
        for (int n = 0; n < groups[i]->getUsers().size(); ++n)
        {
            if (groups[i]->getUsers()[n]->getPower() < user->getPower() && groups[i]->getUsers()[n]->getName() != user->getName())
                this->loadedUsers.push_back(groups[i]->getUsers()[n]);
        }
    }
}

void                MainWindow::loadUsersByFilter(QVector<QPair<QString, QString>> filters)
{
    this->filteredUsers.clear();

    for (int i = 0; i < loadedUsers.size(); ++i)
    {
        bool        match = true;

        for (int n = 0; n < filters.size(); ++n)
        {
            if (filters[n].first == CIT && filters[n].second != loadedUsers[i]->getCity())
                match = false;
            else if (filters[n].first == GRP)
            {
                int group_match = loadedUsers[i]->getGroupNames().size();
                for (int l = 0; l < loadedUsers[i]->getGroupNames().size(); ++l)
                {
                    if (filters[n].second != loadedUsers[i]->getGroupNames()[l])
                        --group_match;
                }
                if (group_match == 0)
                    match = false;
            }
            else if (filters[n].first == TYP)
            {
                int type_match = loadedUsers[i]->getTasks().size();
                for (int l = 0; l < loadedUsers[i]->getTasks().size(); ++l)
                {
                    if (filters[n].second != loadedUsers[i]->getTasks()[l]->getType())
                        --type_match;
                }
                if (type_match == 0)
                    match = false;
            }
        }
        if (match)
            this->filteredUsers.push_back(loadedUsers[i]);
    }
}

void                MainWindow::reloadData(bool value)
{
    QVector<QPair<QString,QString>> filters;

    for (int i = 0; i < checkBoxes.size(); ++i)
    {
        if (checkBoxes[i].checkbox->isChecked())
            filters.push_back(QPair<QString, QString>{checkBoxes[i].type, checkBoxes[i].name});
    }
    this->loadUsersByFilter(filters);
    this->fillUI();
}

void                MainWindow::fillUI()
{
    this->deleteDefaultUI();

    QVector<QPair<QString, int>>    citiesAndNb;
    QVector<QPair<QString, int>>    namesAndNb;
    QVector<QPair<QString, int>>    typeAndNb;
    User                            *user = this->currentUser;

    citiesAndNb.push_back(QPair<QString, int>{"", 0});
    namesAndNb.push_back(QPair<QString, int>{"", 0});
    typeAndNb.push_back(QPair<QString, int>{"", 0});

    for (int i = -1; i < this->filteredUsers.size(); ++i)
    {
        if (i > -1)
            user = filteredUsers[i];

        for (int l = 0; l < citiesAndNb.size(); ++l) {
            if (citiesAndNb[l].first == user->getCity()) {
                ++citiesAndNb[l].second; break; }
            else if (l + 1 >= citiesAndNb.size()) {
                citiesAndNb.push_back(QPair<QString, int>{user->getCity(), 1}); break; } }

        for (int l = 0; l < user->getGroupNames().size(); ++l) {
            for (int n = 0; n < namesAndNb.size(); ++n)
            {
                if (user->getName() == this->currentUser->getName()) {
                    namesAndNb.push_back(QPair<QString, int>{"Myself", 0}); break; }
                else if (namesAndNb[n].first == user->getGroupNames()[l]) {
                    ++namesAndNb[n].second; break; }
                else if (n + 1 >= namesAndNb.size()) {
                    namesAndNb.push_back(QPair<QString, int>{user->getGroupNames()[l], 1}); break; } } }

        for (int l = 0; l < user->getTasks().size(); ++l) {
            for (int n = 0; n < typeAndNb.size(); ++n)
            {
                if (typeAndNb[n].first == user->getTasks()[l]->getType()) {
                    ++typeAndNb[n].second; break; }
                else if (n + 1 >= typeAndNb.size()) {
                    typeAndNb.push_back(QPair<QString, int>{user->getTasks()[l]->getType(), 1}); break; } } }
    }

    citiesAndNb.erase(citiesAndNb.begin());
    namesAndNb.erase(namesAndNb.begin());
    typeAndNb.erase(typeAndNb.begin());

    this->setBoxes(CIT, citiesAndNb);
    this->setBoxes(GRP, namesAndNb);
    this->setBoxes(TYP, typeAndNb);
}

void                MainWindow::deleteDefaultUI()
{
    // Delete checkboxes of UI presentation

    this->clearLayout(this->ui->boxCity->layout());
    this->clearLayout(this->ui->boxGroup->layout());
    this->clearLayout(this->ui->boxType->layout());
    /*delete this->ui->checkBox;
    delete this->ui->checkBox_2;

    delete this->ui->checkBox_3;
    delete this->ui->checkBox_4;
    delete this->ui->checkBox_5;

    delete this->ui->checkBox_6;
    delete this->ui->checkBox_7;
    delete this->ui->checkBox_8;
    delete this->ui->checkBox_9;*/
    //
}

void                MainWindow::clearLayout(QLayout *layout)
{
    QLayoutItem     *item;
    while((item = layout->takeAt(0))) {
        if (item->layout()) {
            clearLayout(item->layout());
            delete item->layout();
        }
        if (item->widget()) {
            delete item->widget();
        }
        delete item;
    }
}

void                MainWindow::setBoxes(QString type, QVector<QPair<QString, int>> namesAndNb)
{
    QGroupBox       *box;

    if (type == CIT)
        box = this->ui->boxCity;
    else if (type == GRP)
        box = this->ui->boxGroup;
    else
        box = this->ui->boxType;

    int             max_col = 2;
    int             col = 0;
    int             row = 0;

    for (int i = 0; i < namesAndNb.size(); ++i)
    {
        QString     str = namesAndNb[i].first + " (" + QString::number(namesAndNb[i].second) + ")";

        if (namesAndNb[i].second == 0)
            str = namesAndNb[i].first;

        QCheckBox   *checkBox = new QCheckBox(str, this);
        QGridLayout *layout = (QGridLayout*)box->layout();
        t_filter    filter = {checkBox, type, namesAndNb[i].first, namesAndNb[i].second};
        checkBoxes.push_back(filter);

        checkBox->setChecked(true);
        connect(checkBox, SIGNAL(clicked(bool)), this, SLOT(reloadData(bool)));

        layout->addWidget(checkBox, row, col);
        if (col + 1 >= max_col)
        {
            ++row;
            col = 0;
        }
        else
            ++col;
    }
}

