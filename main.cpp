#include "mainwindow.h"
#include "log.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow  *w;
    Log         *l;

    QVector<QPair<QString, QString>>    vec;
    QPair<QString,QString>  pair_1 = {"M4ADM", "M4ADM"};
    QPair<QString,QString>  pair_2 = {"M1101", "test"};

    vec.push_back(pair_1);
    vec.push_back(pair_2);

    l = new Log(vec);
    bool value = l->exec();

    if (!value)
        return (0);

    w = new MainWindow(l->getUser());
    w->show();
    return a.exec();
}
