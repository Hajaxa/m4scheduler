#ifndef USER_H
#define USER_H

#include "task.h"
#include <QVector>

class User
{
public:
    User();
    ~User();

    void                setName(QString _name);
    void                setGroupNames(QVector<QString> _groupNames);
    void                setPower(int _power);
    void                setTasks(QVector<Task*> _tasks);
    void                setCity(QString _city);

    void                addGroupName(QString _groupName);
    void                addTask(Task *_task);

    QString             getName();
    QVector<QString>    getGroupNames();
    int                 getPower();
    QVector<Task*>      getTasks();
    QString             getCity();

private:
    QString             name;
    QVector<QString>    groupNames;
    int                 power = 0;
    QVector<Task*>      tasks;
    QString             city;
};

#endif // USER_H
