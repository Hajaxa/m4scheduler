#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <iostream>
#include <QCheckBox>
#include "group.h"

typedef struct  s_filter
{
    QCheckBox   *checkbox;
    QString     type;
    QString     name;
    int         nb;
}               t_filter;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QString username, QWidget *parent = 0);
    ~MainWindow();

    void                generateTestData();
    void                parseData();
    void                outputData();

    void                start(QString username);
    User*               findUser(QString username);
    void                loadWeakerUsers(User *user);
    void                loadUsersByFilter(QVector<QPair<QString, QString>> filters);

    void                fillUI();
    void                deleteDefaultUI();
    void                clearLayout(QLayout *layout);
    void                setBoxes(QString type, QVector<QPair<QString, int>> namesAndNb);

private slots:
    void                reloadData(bool value);

private:
    Ui::MainWindow      *ui;
    QVector<Group*>     groups;
    QVector<User*>      loadedUsers;
    QVector<User*>      filteredUsers;
    User                *currentUser;
    QVector<t_filter>   checkBoxes;
};

#endif // MAINWINDOW_H
